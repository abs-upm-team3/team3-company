package org.xploration.team3.company;

import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.core.*;
import jade.core.behaviours.*;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.*;
import jade.content.onto.*;
import jade.content.onto.basic.*;

import org.xploration.ontology.*;
import org.xploration.team3.common.Constants;

public class Company extends Agent {

	private static final long serialVersionUID = -3989737101882368246L;

	private Codec codec = new SLCodec();
	private Ontology ontology = XplorationOntology.getInstance();

	@Override
	public void setup() {
		System.out.println(getLocalName() + ": has entered into the system");
		getContentManager().registerLanguage(codec);
		getContentManager().registerOntology(ontology);

		addBehaviour(registerCompany());
	}

	private Behaviour registerCompany() {

		return new SimpleBehaviour(this) {

			private static final long serialVersionUID = 7134454796490752122L;
			private boolean requestedRegistration = false;

			@Override
			public boolean done() {
				// TODO Auto-generated method stub
				return requestedRegistration;
			}

			@Override
			public void action() {
				DFAgentDescription dfd = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType(Constants.REGISTRATIONDESK);
				dfd.addServices(sd);

				try {
					// It finds agents of the required type
					DFAgentDescription[] res = new DFAgentDescription[20];
					res = DFService.search(myAgent, dfd);

					// Gets the first occurrence, if there was success
					if (res.length > 0) {
						AID registrationDeskAgent = res[0].getName();

						// Asks the estimation to the painter
						ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
						msg.addReceiver(registrationDeskAgent);
						msg.setLanguage(codec.getName());
						msg.setOntology(ontology.getName());
						// RegistrationRequest regRequest = new
						// DefaultRegistrationRequest();
						// Team ownTeam = new DefaultTeam();
						RegistrationRequest regRequest = new RegistrationRequest();
						Team ownTeam = new Team();
						ownTeam.setTeamId(Constants.teamId);
						regRequest.setTeam(ownTeam);
						// As it is an action and the encoding language the SL,
						// it must be wrapped
						// into an Action
						Action agAction = new Action(registrationDeskAgent, regRequest);
						try {
							// The ContentManager transforms the java objects
							// into strings
							getContentManager().fillContent(msg, agAction);
							send(msg);
							System.out.println(getLocalName() + ": REQUESTS A REGISTRATION");

							requestedRegistration = true;
						} catch (CodecException | OntologyException e) {
							e.printStackTrace();
							doWait(10000);
						}
					} else {
						// If no RegistrationDesk has been found, wait 5 seconds
						doWait(5000);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		};
	}

}
